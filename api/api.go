package api

import (
    "golang.org/x/oauth2"
    "github.com/google/go-github/github"
    "encoding/json"
    "io/ioutil"
    "fmt"
)

/*
* Package: GoLang Github API Client for querying Github API
* Author: Michael Baldwin
*/

type Request struct {
    Limit int `json:"limit"`
    Count int `json:"count"`
}

type Config struct {
    ApiKey string `json:"apikey"`
    Request *Request `json:"request"`
}

type Api struct {
    Config *Config
    Client *github.Client
}

/*
 * Create a new API client
 */
func New() (a *Api) {
    return new(Api)
}

/*
 * Configure the client for a private API key.
 *
 * - http://stackoverflow.com/questions/16465705/how-to-handle-configuration-in-go
 * - http://stackoverflow.com/questions/16681003/how-do-i-parse-a-json-file-into-a-struct-with-go
 */
func (a *Api) Configure(fileName string) {

    // try to read file
    file, fileErr := ioutil.ReadFile(fileName)
    if fileErr != nil {
        fmt.Errorf("Error reading file: ", fileErr)
    }

    // create a config object to hold the json data
    var configuration Config

    // unmarshal the json
    unmarshalErr := json.Unmarshal(file, &configuration)
    if unmarshalErr != nil {
        fmt.Errorf("Error unmarshaling json: ", unmarshalErr)
    }

    // set the config object
    a.Config = &configuration
}

/*
 * Authenticate go-github client using Personal Access Token
 *
 * - https://godoc.org/github.com/google/go-github/github#Authentication
 * - https://godoc.org/github.com/google/go-github/github#NewClient
 */
func (a *Api) Authenticate(key string) {

    // oauth2 authentication using Personal Access Token from Github
    ts := oauth2.StaticTokenSource(
        &oauth2.Token{AccessToken: key},
    )
    tc := oauth2.NewClient(oauth2.NoContext, ts)

    // go-github client for requesting data from Github API
    a.Client = github.NewClient(tc)
}
