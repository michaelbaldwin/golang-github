# golang-github
Library for querying Github API through GoLang Github API Client.

## Config File
- Location: Root directory
- Name and Extension: 'githubConfig.json'
- Format:
```
{
    "apikey": "insert your personal access token here",
    "request": {
        "limit": maxNumRequests
    }
}
```
