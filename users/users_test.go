package users

import (
    "testing"
    "github.com/baldwmic/golang-github/api"
)

const configFile = "../githubConfig.json"

/*
 * Get users that user is following
 */
func TestGetFollowing(t *testing.T) {

    // configure and authenticate api client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get users that user is following
    users := GetFollowing(a, "BerkeleyTrue")
    t.Log(len(users))
    /*
    for i := 0; i < len(users); i++ {
        t.Log(users[i])
    }
    */
}

/*
 * Get followers of a user
 */
func TestGetFollowers(t *testing.T) {

    // configure and authenticate api client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get users that user is following
    users := GetFollowers(a, "BerkeleyTrue")
    t.Log(len(users))
    /*
    for i := 0; i < len(users); i++ {
        t.Log(users[i])
    }
    */
}
