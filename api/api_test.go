package api

import (
    "testing"
)

/*
* Package: GoLang API Client
* Author: Michael Baldwin
*/

const configFile = "../githubConfig.json"

/*
* Test creating new Api
*/
func TestNew(t *testing.T) {

    // create new api
    a := New()

    // client should be nil
    if a.Client != nil {
        t.Errorf("New(), Api.client == %q, expected %q", a.Client, nil)
    }
}

/*
 * Test configuring the API Client for a private key.
 */
func TestConfigure(t *testing.T) {

    // create new api
    a := New()

    // configure client
    a.Configure(configFile)

    // test to see if data loaded
    t.Log(a.Config.ApiKey)
    t.Log(a.Config.Request)
}

/*
* Test authenticating Github API client
*/
func TestAuthenticate(t *testing.T) {

    // create new api
    a := New()

    // configure client
    a.Configure(configFile)

    // authenticate client
    a.Authenticate(a.Config.ApiKey)

    // client should not be nil
    if a.Client == nil {
        t.Errorf("Authenticate(), Api.client == %q, expected %q", nil, "*github.Client")
    }

    t.Log(a.Client.RateLimits())
}
