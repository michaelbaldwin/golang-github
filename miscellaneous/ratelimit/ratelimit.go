package ratelimit

import (
    "github.com/baldwmic/golang-github/api"
    "github.com/google/go-github/github"
    "time"
    "fmt"
)

/*
 * Get the rate limits of a connected client.
 * https://godoc.org/github.com/google/go-github/github#Client.RateLimits
 */
func GetRateLimits(a *api.Api) (*github.RateLimits, error) {

    // get the current rate limits for the authenticated Go-Github client.
    rateLimits, _, err := a.Client.RateLimits()
    if err != nil {
        fmt.Errorf("Error fetching rate limit: %#v\n\n", err)
	return nil, err
    }
    return rateLimits, nil
}

/*
 * Get the number of core requests remaining.
 */
func GetCoreRemaining(a *api.Api) (int, error) {
    rateLimits, err := GetRateLimits(a)
    if err != nil {
	    fmt.Errorf("Error getting core remaining: %v\n\n", err)
        return -1, err
    }
    return rateLimits.Core.Remaining, nil
}

/*
 * Check whether core requests remaining are equal to zero or not. A result of
 * true indicates that core requests have been exceeded for the current hour.
 */
func CheckCoreRemainingConsumed(a *api.Api) (bool, error) {
    remaining, err := GetCoreRemaining(a)
    if err != nil {
        fmt.Errorf("Error checking core remaining consumed: %v\n\n", err)
        // return true if error
        return true, err
    }
    return remaining == 0, nil
}

/*
 * Get the timestamp when request limit will reset
 */
func GetCoreResetTime(a *api.Api) (time.Time, error) {
    rateLimits, err := GetRateLimits(a)
    if err != nil {
	    fmt.Errorf("Error getting core reset time: %v\n\n", err)
        // return one hour from now if error
	    return time.Now().Add(1 * time.Hour), err
    }
    return rateLimits.Core.Reset.Time, nil
}

/*
 * Sleep Goroutine (lightweight thread) when request limit reached. Sleep until
 * core request limit resets.
 * https://developer.github.com/v3/#rate-limiting
 */
func HandleRateLimits(a *api.Api) {

    // if requests consumed, handle by sleeping until limit resets
    consumed, err := CheckCoreRemainingConsumed(a)
    if err != nil {
        fmt.Errorf("Error checking whether core requests remaining consumed: %v\n\n", err)
    } else {
        if consumed {
            fmt.Println("Rate limit consumed for current rate limit window.")

            // get time in future when core request limit will reset
            resetTime, err := GetCoreResetTime(a)
            if err != nil {
		        fmt.Errorf("Error getting core reset time: %v\n\n", err)
	        } else {

                // add time to safely avoid requesting before rate limit resets
            	resetTime = resetTime.Add(1 * time.Minute)

            	// get current time
            	currentTime := time.Now()

            	// calculate time to wait until rate limit resets
            	// resetTime - currentTime = waitDuration
            	waitDuration := resetTime.Sub(currentTime)

            	// sleep Goroutine until time when rate limit will be reset
            	fmt.Printf("Current time: %v\n", currentTime)
            	fmt.Printf("Future time when rate limit will reset: %v\n", resetTime)
            	fmt.Printf("Sleeping Goroutine for: %v\n", waitDuration)
            	time.Sleep(waitDuration)
    	    }
     	}
    }
}
