package users

import (
    "github.com/baldwmic/golang-github/api"
    "github.com/google/go-github/github"
    "github.com/baldwmic/golang-github/miscellaneous/ratelimit"
    "fmt"
)

/*
* Get those users that a user is following
* - https://developer.github.com/v3/users/followers/#list-users-followed-by-another-user
* - https://godoc.org/github.com/google/go-github/github#UsersService.ListFollowing
*/
func GetFollowing(a *api.Api, username string) []github.User {

    // Github API allows max of 100 results per page
    listOptions := &github.ListOptions{PerPage: 100, Page: 1}

    // get all pages of results
    var allFollowing []github.User
    for {

        // if rate limit for core requests remaining exceeded, exit
        ratelimit.HandleRateLimits(a)

        // get the users that the user is following and increment the request count
        following, resp, err := a.Client.Users.ListFollowing(username, listOptions)
        a.Config.Request.Count += 1

        if err != nil {
            fmt.Errorf("Error getting following: %#v\n\n", err)
        } else {
            allFollowing = append(allFollowing, following...)
        }

        // exit the loop since no more pages
        if resp.NextPage == 0 {
            break
        }
        // continue to traverse through paginated results
        listOptions.Page = resp.NextPage
    }
    return allFollowing
}

/*
* Get followers of a user
* - https://developer.github.com/v3/users/followers/#list-followers-of-a-user
* - https://godoc.org/github.com/google/go-github/github#UsersService.ListFollowers
*/
func GetFollowers(a *api.Api, username string) []github.User {

    // Github API allows max of 100 results per page
    listOptions := &github.ListOptions{PerPage: 100, Page: 1}

    // get all pages of results
    var allFollowers []github.User
    for {

        // if rate limit for core requests remaining exceeded, exit
        ratelimit.HandleRateLimits(a)

        // get the users that the user is following and increment the request count
        followers, resp, err := a.Client.Users.ListFollowers(username, listOptions)
        a.Config.Request.Count += 1

        if err != nil {
            fmt.Errorf("Error getting following: %#v\n\n", err)
        } else {
            allFollowers = append(allFollowers, followers...)
        }

        // exit the loop since no more pages
        if resp.NextPage == 0 {
            break
        }
        // continue to traverse through paginated results
        listOptions.Page = resp.NextPage
    }
    return allFollowers
}
