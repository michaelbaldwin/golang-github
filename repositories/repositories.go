package repositories

import (
    "github.com/baldwmic/golang-github/api"
    "github.com/google/go-github/github"
    "github.com/baldwmic/golang-github/miscellaneous/ratelimit"
    "fmt"
)

/*
 * Get list of contributors of an owner's repository
 * https://developer.github.com/v3/repos/#list-contributors
 * https://godoc.org/github.com/google/go-github/github#ListOptions
 * https://godoc.org/github.com/google/go-github/github#ListContributorsOptions
 * https://godoc.org/github.com/google/go-github/github#hdr-Pagination
 */
func GetListContributors(a *api.Api,
                         owner string,
                         repository string) []github.Contributor {

    contributorOptions := &github.ListContributorsOptions{
        // ignore anonymous contributors - Github API specifies only first 500
        // author email addresses in repository will be linked to Github users
        Anon: "omitempty",
        // Github API allows max of 100 results per page
        ListOptions: github.ListOptions{PerPage: 100, Page: 1},
    }

    // get all pages of results
    var allContributors []github.Contributor
    for {

        // if rate limit for core requests remaining exceeded, exit
        ratelimit.HandleRateLimits(a)

        // get the list of contributors and increment the request count
        contributors, resp, err := a.Client.Repositories.ListContributors(owner,
            repository, contributorOptions)
        a.Config.Request.Count += 1

        if err != nil {
            fmt.Errorf("Error getting contributors: %#v\n\n", err)
        } else {
            // ellipsis indicates appending each element in contributors slice
            // to allContributors slice, instead of append slice onto slice
            allContributors = append(allContributors, contributors...)
        }

        // exit the loop since no more pages
        if resp.NextPage == 0 {
            break
        }
        // continue to traverse through paginated results
        contributorOptions.ListOptions.Page = resp.NextPage
    }
    return allContributors
}
