package ratelimit

import (
    "testing"
    "github.com/baldwmic/golang-github/api"
)

const configFile = "../../githubConfig.json"

/*
 * Test the rate limits of a connected client
 */
func TestGetRateLimits(t *testing.T) {

    // configure and authenticate client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get rate limits
    t.Log(GetRateLimits(a))
}

/*
 * Test getting the number of requests remaining
 */
func TestGetCoreRemaining(t *testing.T) {

    // configure and authenticate client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get core requests remaining
    t.Log(GetCoreRemaining(a))
}

/*
 * Test checking the number of requests remaining has or has not been exceeded
 */
func TestCheckCoreRemaining(t *testing.T) {

    // configure and authenticate client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get core requests remaining
    t.Log(CheckCoreRemainingConsumed(a))
}

/*
 * Test getting the timestamp when core request limit will reset.
 */
func TestGetCoreResetTime(t *testing.T) {

    // configure and authenticate client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get the timestamp when core request limit will reset.
    t.Log(GetCoreResetTime(a))
}

/*
 * Test handling request limits
 */
func TestHandleRateLimits(t *testing.T) {

    // create, configure, authenticate client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // handle rate limits
    HandleRateLimits(a)
}
