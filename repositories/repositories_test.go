package repositories

import (
    "testing"
    "github.com/baldwmic/golang-github/api"
)

const configFile = "../githubConfig.json"

/*
 * Get list of contributors for a repository
 */
func TestGetListContributors(t *testing.T) {

    // configure and authenticate api client
    a := api.New()
    a.Configure(configFile)
    a.Authenticate(a.Config.ApiKey)

    // get list of contributors for a owner's repository
    contributors := GetListContributors(a, "FreeCodeCamp", "FreeCodeCamp")
    t.Log(len(contributors))
    /*
    for i := 0; i < len(contributors); i++ {
        t.Log(contributors[i])
    }
    */
}
